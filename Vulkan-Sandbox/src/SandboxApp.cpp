#include "VkCore.h"
#include <memory>

using namespace VkCore;

class Sandbox : public Application {
public:
    Sandbox() {}
};

int main() {
    VkCore::Log::Init();
    LOG_WARN("Initialized Log!");
    int a = 5;
    LOG_INFO("Hello Var={0}", a);
    std::unique_ptr<Sandbox> app = std::make_unique<Sandbox>();
    app->Run();

    return 0;
}
