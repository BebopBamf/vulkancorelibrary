# Vulkan Core Library

This is a Vulkan Base Library for writing small sandboxed vulkan examples

A lot of this code was taken directly from the cherno's opengl core library https://github.com/TheCherno/OpenGL
and was modified slightly. Changes to his original code was changing the build system to cmake and replacing opengl
specific code with vulkan code.
