#pragma once

#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

namespace VkCore {
    class Log {
    public:
        static void Init();

        inline static std::shared_ptr<spdlog::logger>& GetLogger() { return s_Logger; };

    private:
        static std::shared_ptr<spdlog::logger> s_Logger;
    };
}

#define LOG_TRACE(...)         ::VkCore::Log::GetLogger()->trace(__VA_ARGS__)
#define LOG_INFO(...)          ::VkCore::Log::GetLogger()->info(__VA_ARGS__)
#define LOG_WARN(...)          ::VkCore::Log::GetLogger()->warn(__VA_ARGS__)
#define LOG_ERROR(...)         ::VkCore::Log::GetLogger()->error(__VA_ARGS__)
#define LOG_CRITICAL(...)      ::VkCore::Log::GetLogger()->critical(__VA_ARGS__)