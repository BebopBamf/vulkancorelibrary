#pragma once

namespace VkCore {
    class Application {
    public:
        Application();
        virtual ~Application();

        void Run();
    };
}
